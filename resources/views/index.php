<html>
<body>
<h1>Таксопарк</h1>
<ul>
    <li><a href="<?= route('companies'); ?>">Список компаний</a></li>
    <li><a href="<?= route('cars'); ?>">Список машин</a></li>
    <li><a href="<?= route('drivers'); ?>">Список водителей</a></li>
    <li><a href="<?= route('employees'); ?>">Список служащих</a></li>
    <li><a href="<?= route('stations'); ?>">Список парковок</a></li>
    <li><a href="<?= route('arrivals'); ?>">Список прибытий</a></li>
    <li><a href="<?= route('leavings'); ?>">Список отъездов</a></li>
</ul>
</body>
</html>