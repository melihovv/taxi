<html>
<body>
<h1>Список компаний</h1>
<ul data-url="companies">
    <?php
    foreach ($companies as $company) {
        echo <<<EOD
<li data-id="$company->id">
    <input value="$company->name">
    <button class="update">Обновить</button>
    <button class="remove">Удалить</button>
</li>
EOD;
    }
    ?>
</ul>
<h1>Добавить новую компанию</h1>
<input type="text">
<button class="add">Добавить</button>

<script src="/public/vendor/jquery/dist/jquery.min.js"></script>
<script>
    $(function () {
        var $ul = $('ul');
        var url = $ul.data('url');
        var $addBtn = $('.add');

        $addBtn.click(function (e) {
            var name = $(e.target).prev('input').val();
            if (name.length < 1) {
                return;
            }
            $.ajax({
                url: '/public/' + url + '/add',
                method: 'POST',
                data: 'name=' + name
            }).done(function (res) {
                var id = res.id;
                $ul.append('<li data-id="' + id + '">' +
                    '<input value="' + name + '"> ' +
                    '<button class="update">Обновить</button> ' +
                    '<button class="remove">Удалить</button>' +
                    '</li>');
            });
        });

        $ul.click(function (e) {
            var $target = $(e.target);
            var $li = $target.parent();
            var id = $li.data('id');

            switch (e.target.className) {
                case 'update':
                    var $input = $target.prev('input');
                    var newName = $input.val();
                    $.ajax({
                        url: '/public/' + url + '/update/' + id,
                        method: 'POST',
                        data: 'name=' + newName
                    });
                    break;
                case 'remove':
                    $.ajax({
                        url: '/public/' + url + '/remove/' + id,
                        method: 'POST'
                    }).done(function (res) {
                        if (res.removed >= 1) {
                            $li.remove();
                        }
                    });
                    break;
                default:
                    break;
            }
        });
    });
</script>
</body>
</html>
