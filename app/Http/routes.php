<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', [
    'uses' => 'TaxiController@index',
    'as' => 'index'
]);

$app->get('/companies', [
    'uses' => 'TaxiController@companies',
    'as' => 'companies'
]);

$app->post('/companies/remove/{id}', [
    'uses' => 'TaxiController@removeCompany'
]);

$app->post('/companies/update/{id}', [
    'uses' => 'TaxiController@updateCompany'
]);

$app->post('/companies/add', [
    'uses' => 'TaxiController@addCompany'
]);

$app->get('/cars', [
    'uses' => 'TaxiController@cars',
    'as' => 'cars'
]);

$app->get('/drivers', [
    'uses' => 'TaxiController@drivers',
    'as' => 'drivers'
]);

$app->get('/employees', [
    'uses' => 'TaxiController@employees',
    'as' => 'employees'
]);

$app->get('/stations', [
    'uses' => 'TaxiController@stations',
    'as' => 'stations'
]);

$app->get('/arrivals', [
    'uses' => 'TaxiController@arrivals',
    'as' => 'arrivals'
]);

$app->get('/leavings', [
    'uses' => 'TaxiController@leavings',
    'as' => 'leavings'
]);
