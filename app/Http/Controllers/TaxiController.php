<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;


class TaxiController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function companies()
    {
        $companies = DB::table('companies')->get();
        return view('companies', ['companies' => $companies]);
    }

    public function removeCompany($id)
    {
        $removed = DB::table('companies')
            ->where('id', $id)
            ->delete();
        return response()->json(['removed' => $removed]);
    }

    public function updateCompany(Request $req, $id)
    {
        $newName = $req->input('name');
        $affected = DB::table('companies')
            ->where('id', $id)
            ->update(['name' => $newName]);
        return response()->json(['affected' => $affected]);
    }

    public function addCompany(Request $req)
    {
        $name = $req->input('name');
        $id = DB::table('companies')->insertGetId([
            'name' => $name
        ]);
        return response()->json(['id' => $id]);
    }

    public function cars()
    {
        return view('cars');
    }

    public function drivers()
    {
        return view('drivers');
    }

    public function employees()
    {
        return view('employees');
    }

    public function stations()
    {
        return view('stations');
    }

    public function arrivals()
    {
        return view('arrivals');
    }

    public function leavings()
    {
        return view('leavings');
    }
}